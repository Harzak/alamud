from .action import Action2
from mud.events import EquipEvent

class EquipAction(Action2):
    EVENT = EquipEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "equip"
