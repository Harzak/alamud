# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action1
from mud.events import ToquerEvent
import mud.game


class ToquerAction(Action1):
    EVENT = ToquerEvent
    ACTION = "toquer"

