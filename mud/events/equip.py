from .event import Event2

class EquipEvent(Event2):
    NAME = "equip"

    def perform(self):
        self.inform("equip")
